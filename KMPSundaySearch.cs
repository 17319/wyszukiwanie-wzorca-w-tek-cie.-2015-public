﻿/*
 * Created by SharpDevelop.
 * User: b
 * Date: 2015-03-31
 * Time: 14:17
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;

namespace ALG1PROJSD
{
	/// <summary>
	/// Description of KMPSundaySearch.
	/// </summary>
	public class KMPSundaySearch:StringSearch
	{
		public KMPSundaySearch(string text, string pattern, bool czyPokazacWyniki)
			: base(text, pattern,czyPokazacWyniki)
		{
		}
		
		public  List<int> find()
		{
			occurences = new List<int>();
			List<int> tabS = TabSunday(pattern);
			List<int> tabK = TabKMP(pattern);
			int i=0,j=0,przeskok;
			while (i < n) {
				while (j >= 0 && text[i] != pattern[j]) {
					przeskok = i + m - j - tabS[text[i+m-j]];
					if (przeskok > i && przeskok <= n) {
						i = przeskok;
						if (matchesAt(i)) {
							occurences.Add(i);
							i += m;
						}
						j = 0;
					} else {
						j = tabK[j];
					}
				}
				i++;
				j++;
				if (j == m) {
					occurences.Add(i-m);
					j = tabK[m];
				}
			}
			
			
			return occurences;
			if (czyPokazacWyniki)
			{
				Console.WriteLine("Wyniki: ");
				show();
			}
		}
		
		
		public  List<int> TabSunday(string pattern)
		{
			List<int> TabS = new List<int>(256);
			for (int i=0;i<256;i++)
			{
				TabS.Add(-1);
			}
			for (int i = 0; i <pattern.Length; i++)
			{
				TabS[pattern[i]] = i;
			}
			return TabS;
		}
		
		public List<int> TabKMP(string pattern)
		{
			List<int> TabKMP = new List<int>(pattern.Length+1);
			
			int i=0,j=-1;
			TabKMP.Add(-1);
			while (i < pattern.Length) {
				while(j > -1 && pattern[i] != pattern[j]) {
					j = TabKMP[j];
				}
				i++;
				j++;
				if (pattern[j] == pattern[i-1]) {
					TabKMP.Add(TabKMP[j]);
				} else {
					TabKMP.Add(j);
				}
			}
			return TabKMP;
		}
	}
}
