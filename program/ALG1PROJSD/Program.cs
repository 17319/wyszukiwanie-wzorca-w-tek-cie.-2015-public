﻿/*
 * Created by SharpDevelop.
 * User: b
 * Date: 2015-03-16
 * Time: 05:41
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;

namespace ALG1PROJSD
{
	class Program
	{
		public static void Main(string[] args)
		{
            
			string text,pattern;
			const bool czyPokazywacWyniki = false;
			#region usuwamy plik z odpowiedziami
			System.IO.FileInfo fi = new System.IO.FileInfo(@"odpowiedzi.txt");
            try
            {
                fi.Delete();
            }
            catch (Exception)
            {
                ;
            }
            #endregion
            kreator();

            #region listyPomocnicze
            List<string> listMowa = new List<string>();
            listMowa.Add("mowa10000000.txt");
            listMowa.Add("mowa9000000.txt");
            listMowa.Add("mowa8000000.txt");
            listMowa.Add("mowa7000000.txt");
            listMowa.Add("mowa6000000.txt");
            listMowa.Add("mowa5000000.txt");
            listMowa.Add("mowa4000000.txt");
            listMowa.Add("mowa3000000.txt");
            listMowa.Add("mowa2000000.txt");
            listMowa.Add("mowa1000000.txt");
            listMowa.Add("mowa100000.txt");
            listMowa.Add("mowa10000.txt");
            List<string> listDna = new List<string>();
            listDna.Add("dna10000000.txt");
            listDna.Add("dna9000000.txt");
            listDna.Add("dna8000000.txt");
            listDna.Add("dna7000000.txt");
            listDna.Add("dna6000000.txt");
            listDna.Add("dna5000000.txt");
            listDna.Add("dna4000000.txt");
            listDna.Add("dna3000000.txt");
            listDna.Add("dna2000000.txt");
            listDna.Add("dna1000000.txt");
            listDna.Add("dna100000.txt");
            listDna.Add("dna10000.txt");
            List<string> listDnaLen = new List<string>();
            listDnaLen.Add("dna6Pattern.txt");
            listDnaLen.Add("dna60Pattern.txt");
            listDnaLen.Add("dna600Pattern.txt");
            listDnaLen.Add("dna6000Pattern.txt");
            List<string> listMowaLen = new List<string>();
            listMowaLen.Add("mowa6Pattern.txt");
            listMowaLen.Add("mowa60Pattern.txt");
            listMowaLen.Add("mowa600Pattern.txt");
            listMowaLen.Add("mowa6000Pattern.txt");
            #endregion
            #region testowalnia
            for (int odp = 1; odp <= 3; odp++)
            {
                if (odp == 1)
                {
                    foreach (var t in listMowa)
                        foreach (var p in listMowaLen)
                        {
                            text = System.IO.File.ReadAllText(@t);
                            pattern = System.IO.File.ReadAllText(@p);
                            tester(text, pattern, czyPokazywacWyniki, odp);
                        }
                }
                else if (odp == 2)
                {
                    foreach (var t in listDna)
                        foreach (var p in listDnaLen)
                        {
                            text = System.IO.File.ReadAllText(@t);
                            pattern = System.IO.File.ReadAllText(@p);
                            tester(text, pattern, czyPokazywacWyniki, odp);
                        }

                }
                else if (odp == 3)
                {
                    foreach (var t in listMowa)
                        foreach (var p in listMowaLen)
                        {
                            string pomDna = System.IO.File.ReadAllText(@t);
                            string pomDnaLen = System.IO.File.ReadAllText(@p);
                            StreamWriter los1;
                            StreamWriter los1Pattern = new StreamWriter(@"los1Pattern.txt", false);
                            los1Pattern.WriteLine(Generator.GetRandomString(pomDnaLen.Length - 2));
                            los1Pattern.Close();
                            if (pomDnaLen.Length == 6)
                            {
                                los1 = new StreamWriter(@"los1.txt", false);
                                los1.WriteLine(Generator.GetRandomString(pomDna.Length - 2));
                                los1.Close();
                            }
                            text = System.IO.File.ReadAllText(@"los1.txt");
                            pattern = System.IO.File.ReadAllText(@"los1Pattern.txt");
                            tester(text, pattern, czyPokazywacWyniki, odp);
                        }
                }
            }
            Console.WriteLine("Wyniki poleciały do pliku wyniki.txt");
            #endregion
            sprzatacz();
			Console.Write("Press any key to continue . . . ");
			Console.ReadKey(true);
		}
		
		public static void tester(string text, string pattern,
		                          bool czyPokazywacWyniki,int odp)
		{
			StreamWriter wyniki = new StreamWriter(@"odpowiedzi.txt",true);
			if (odp==1)
			{
				wyniki.Write("alfabet mówiony\t");
				Console.WriteLine("mowa  text: " +  text.Length + " pattern: " +  pattern.Length);
			}
			if (odp==2)
			{
				wyniki.Write("alfabet ubogi\t");
				Console.WriteLine("dna    text: " +  text.Length + " pattern: " +  pattern.Length);
			}
			if (odp==3)
			{
				wyniki.Write("ciągi losowe\t");
				Console.WriteLine("los    text: " +  text.Length + " pattern: " +  pattern.Length);
			}
			SundaySearch ss = new SundaySearch(text,pattern,czyPokazywacWyniki);
			KMPSearch kmp = new KMPSearch(text,pattern,czyPokazywacWyniki);
			MPSearch mp = new MPSearch(text,pattern,czyPokazywacWyniki);
			SundayTwoSearch s2 = new SundayTwoSearch(text,pattern,czyPokazywacWyniki);

			Stopwatch sw = new Stopwatch();
			sw.Reset();
			sw.Start();
			ss.find();
			sw.Stop();
			Console.WriteLine("Sunday Takty: " + sw.ElapsedTicks); // kontrola
			wyniki.Write("Sunday\t" +  sw.ElapsedTicks);
			sw.Reset();
			sw.Start();
			kmp.find();
			sw.Stop();
			Console.WriteLine("KMP Takty: " + sw.ElapsedTicks); //kontrola
			wyniki.Write("\tKMP: \t" +  sw.ElapsedTicks);
			sw.Reset();
			sw.Start();
			mp.find();
			sw.Stop();
			Console.WriteLine("MP Takty: " + sw.ElapsedTicks);			wyniki.Write("\tMP: \t" +  sw.ElapsedTicks);
			sw.Reset();
			sw.Start();
			s2.find();
			sw.Stop();
			Console.WriteLine("Sunday2 Takty: " + sw.ElapsedTicks + "\n");	
			System.Threading.Thread.Sleep(200);
			wyniki.Write("\tSunday2: \t" +  sw.ElapsedTicks);
			wyniki.Write("\tText dł: \t" + text.Length);
			wyniki.Write("\tPattern dł: \t" + pattern.Length);
			wyniki.Write("\n");
			System.Threading.Thread.Sleep(2000);
			
			wyniki.Close();
		}	
		public static void kreator()
		{
			Console.WriteLine("Proszę czekać, tworzone są pliki tekstowe, może to chwile potrwać...");
			string dna = System.IO.File.ReadAllText(@"dna10000000.txt");
			StreamWriter dnaSW = new StreamWriter(@"dna9000000.txt",false);
			dnaSW.WriteLine(dna.Substring(0,9000000));
			dnaSW = new StreamWriter(@"dna8000000.txt",false);
			dnaSW.WriteLine(dna.Substring(0,8000000));
			dnaSW = new StreamWriter(@"dna7000000.txt",false);
			dnaSW.WriteLine(dna.Substring(0,7000000));
			dnaSW = new StreamWriter(@"dna6000000.txt",false);
			dnaSW.WriteLine(dna.Substring(0,6000000));
			dnaSW = new StreamWriter(@"dna5000000.txt",false);
			dnaSW.WriteLine(dna.Substring(0,5000000));
			dnaSW = new StreamWriter(@"dna4000000.txt",false);
			dnaSW.WriteLine(dna.Substring(0,4000000));
			dnaSW = new StreamWriter(@"dna3000000.txt",false);
			dnaSW.WriteLine(dna.Substring(0,3000000));
			dnaSW = new StreamWriter(@"dna2000000.txt",false);
			dnaSW.WriteLine(dna.Substring(0,2000000));
			dnaSW = new StreamWriter(@"dna1000000.txt",false);
			dnaSW.WriteLine(dna.Substring(0,1000000));
			dnaSW = new StreamWriter(@"dna100000.txt",false);
			dnaSW.WriteLine(dna.Substring(0,100000));
			dnaSW = new StreamWriter(@"dna10000.txt",false);
			dnaSW.WriteLine(dna.Substring(0,10000));
			dna = File.ReadAllText(@"mowa10000000.txt");
			dnaSW = new StreamWriter(@"mowa9000000.txt",false);
			dnaSW.WriteLine(dna.Substring(0,9000000));
			dnaSW = new StreamWriter(@"mowa8000000.txt",false);
			dnaSW.WriteLine(dna.Substring(0,8000000));
			dnaSW = new StreamWriter(@"mowa7000000.txt",false);
			dnaSW.WriteLine(dna.Substring(0,7000000));
			dnaSW = new StreamWriter(@"mowa6000000.txt",false);
			dnaSW.WriteLine(dna.Substring(0,6000000));
			dnaSW = new StreamWriter(@"mowa5000000.txt",false);
			dnaSW.WriteLine(dna.Substring(0,5000000));
			dnaSW = new StreamWriter(@"mowa4000000.txt",false);
			dnaSW.WriteLine(dna.Substring(0,4000000));
			dnaSW = new StreamWriter(@"mowa3000000.txt",false);
			dnaSW.WriteLine(dna.Substring(0,3000000));
			dnaSW = new StreamWriter(@"mowa2000000.txt",false);
			dnaSW.WriteLine(dna.Substring(0,2000000));
			dnaSW = new StreamWriter(@"mowa1000000.txt",false);
			dnaSW.WriteLine(dna.Substring(0,1000000));
			dnaSW = new StreamWriter(@"mowa100000.txt",false);
			dnaSW.WriteLine(dna.Substring(0,100000));
			dnaSW = new StreamWriter(@"mowa10000.txt",false);
			dnaSW.WriteLine(dna.Substring(0,10000));
			dnaSW.Close();
		}
		public static void czyAlgorytmyDobrzeSzukaja(string t, string p)
		{
			
			SundaySearch ss = new SundaySearch(t,p,true);
			KMPSearch kmp = new KMPSearch(t,p,true);
			MPSearch mp = new MPSearch(t,p,true);
			SundayTwoSearch s2 = new SundayTwoSearch(t,p,true);
			ss.find();
			kmp.find();
			mp.find();
			s2.find();
			System.Threading.Thread.Sleep(2000);
		}
		public static void sprzatacz()
		{
			try{
				System.IO.FileInfo fi = new System.IO.FileInfo(@"dna9000000.txt");
				fi.Delete();
				fi = new System.IO.FileInfo(@"dna8000000.txt");
				fi.Delete();
				fi = new System.IO.FileInfo(@"dna7000000.txt");
				fi.Delete();
				fi = new System.IO.FileInfo(@"dna6000000.txt");
				fi.Delete();
				fi = new System.IO.FileInfo(@"dna5000000.txt");
				fi.Delete();
				fi = new System.IO.FileInfo(@"dna4000000.txt");
				fi.Delete();
				fi = new System.IO.FileInfo(@"dna3000000.txt");
				fi.Delete();
				fi = new System.IO.FileInfo(@"dna2000000.txt");
				fi.Delete();
				fi = new System.IO.FileInfo(@"dna1000000.txt");
				fi.Delete();
				fi = new System.IO.FileInfo(@"dna100000.txt");
				fi.Delete();
				fi = new System.IO.FileInfo(@"dna10000.txt");
				fi.Delete();
				fi = new System.IO.FileInfo(@"mowa9000000.txt");
				fi.Delete();
				fi = new System.IO.FileInfo(@"mowa8000000.txt");
				fi.Delete();
				fi = new System.IO.FileInfo(@"mowa7000000.txt");
				fi.Delete();
				fi = new System.IO.FileInfo(@"mowa6000000.txt");
				fi.Delete();
				fi = new System.IO.FileInfo(@"mowa5000000.txt");
				fi.Delete();
				fi = new System.IO.FileInfo(@"mowa4000000.txt");
				fi.Delete();
				fi = new System.IO.FileInfo(@"mowa3000000.txt");
				fi.Delete();
				fi = new System.IO.FileInfo(@"mowa2000000.txt");
				fi.Delete();
				fi = new System.IO.FileInfo(@"mowa1000000.txt");
				fi.Delete();
				fi = new System.IO.FileInfo(@"mowa100000.txt");
				fi.Delete();
				fi = new System.IO.FileInfo(@"mowa10000.txt");
				fi.Delete();
				fi = new System.IO.FileInfo(@"los1.txt");
				fi.Delete();
				fi = new System.IO.FileInfo(@"los1Pattern.txt");
				fi.Delete();
			}
			catch(Exception)
			{
				Console.WriteLine("Nie było co usuwać");
			}
		}
	}
}