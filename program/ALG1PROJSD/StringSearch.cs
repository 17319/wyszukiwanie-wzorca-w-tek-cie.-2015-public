﻿/*
 * Created by SharpDevelop.
 * User: b
 * Date: 2015-02-20
 * Time: 17:07
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mime;
namespace ALG1PROJSD
{
	/// <summary>
	/// Description of StringSearch.
	/// </summary>
	public class StringSearch
	{
		
		protected string text;
		protected string pattern;
		protected int m,n;
		protected List<int> occurences;
		protected List<int> P;
		protected bool czyPokazacWyniki;
		public StringSearch(string text, string pattern, bool czyPokazacWyniki)
		{
			this.text = text;
			this.pattern = pattern;
			this.m = pattern.Length;
			this.n = text.Length;
			this.czyPokazacWyniki = czyPokazacWyniki;
		}

		protected bool matchesAt(int i)
		{

			int j = 0;
			while ((j < m) && (pattern[j] == text[i + j]))
				j++;
			if (j == m)
				return true;
			else
				return false;
		}
		protected void show()
		{
			foreach (var w in occurences)
				Console.Write (" " + w);
			Console.WriteLine();
		}
		
	}
}
