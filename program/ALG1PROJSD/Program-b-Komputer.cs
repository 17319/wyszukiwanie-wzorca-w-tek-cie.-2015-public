﻿/*
 * Created by SharpDevelop.
 * User: b
 * Date: 2015-03-16
 * Time: 05:41
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.IO;
using System.Diagnostics;
using System.Management.Instrumentation;

namespace ALG1PROJSD
{
	class Program
	{
		
		public static void Main(string[] args)
		{
			string dna = System.IO.File.ReadAllText(@"dna10000000.txt");
			StreamWriter dnaSW = new StreamWriter(@"dna9000000.txt",false);
			dnaSW.Write(dna.Substring(0,9000000));
			dnaSW = new StreamWriter(@"dna8000000.txt",false);
			dnaSW.Write(dna.Substring(2000000));
			dnaSW = new StreamWriter(@"dna7000000.txt",false);
			dnaSW.Write(dna.Substring(3000000));
			dnaSW = new StreamWriter(@"dna6000000.txt",false);
			dnaSW.Write(dna.Substring(4000000));
			dnaSW = new StreamWriter(@"dna5000000.txt",false);
			dnaSW.Write(dna.Substring(5000000));
			dnaSW = new StreamWriter(@"dna4000000.txt",false);
			dnaSW.Write(dna.Substring(6000000));
			dnaSW = new StreamWriter(@"dna3000000.txt",false);
			dnaSW.Write(dna.Substring(7000000));
			dnaSW = new StreamWriter(@"dna2000000.txt",false);
			dnaSW.Write(dna.Substring(8000000));
			dnaSW = new StreamWriter(@"dna1000000.txt",false);
			dnaSW.Write(dna.Substring(9000000));
			dnaSW = new StreamWriter(@"dna500000.txt",false);
			dnaSW.Write(dna.Substring(9500000));
			dnaSW = new StreamWriter(@"dna100000.txt",false);
			dnaSW.Write(dna.Substring(9900000));
			dnaSW = new StreamWriter(@"dna50000.txt",false);
			dnaSW.Write(dna.Substring(9950000));
			dnaSW = new StreamWriter(@"dna10000.txt",false);
			dnaSW.Write(dna.Substring(9990000));
			dnaSW = new StreamWriter(@"dna5000.txt",false);
			dnaSW.Write(dna.Substring(9995000));
			dnaSW = new StreamWriter(@"dna1000.txt",false);
			dnaSW.Write(dna.Substring(9999000));
			dnaSW = new StreamWriter(@"dna500.txt",false);
			dnaSW.Write(dna.Substring(9999500));
			dnaSW = new StreamWriter(@"dna100.txt",false);
			dnaSW.Write(dna.Substring(9999900));
			dnaSW = new StreamWriter(@"dna50.txt",false);
			dnaSW.Write(dna.Substring(9999950));
			dnaSW = new StreamWriter(@"dna10.txt",false);
			dnaSW.Write(dna.Substring(9999990));
			
			dnaSW.Close();
			
			
			
			
			int odp=3;
			string text,pattern;
			bool czyPokazywacWyniki = false;
//			Console.WriteLine("Co robimy: \n"+
//			                  "alfabet mówiony - 1\n"+
//			                  "alfabet ubogi - 2\n"+
//			                  "ciągi losowe - 3\n");
			
			//odp = int.Parse(Console.ReadLine());
//			Console.WriteLine("czy pokazywać wyniki true/false \n");
//			if (bool.Parse(Console.ReadLine()))
//				czyPokazywacWyniki = true;
//			else
//				czyPokazywacWyniki = false;
			
			for (int i=0;i<50;i++)
			{
			if (odp==1)
			{
				text = System.IO.File.ReadAllText(@"mowa1.txt");
				pattern = System.IO.File.ReadAllText(@"mowa1Pattern.txt");						
				tester(text,pattern,czyPokazywacWyniki,odp);
			}
			else if (odp==2)
			{
				text = System.IO.File.ReadAllText(@"dna10000000");
				pattern = System.IO.File.ReadAllText(@"dna1Pattern.txt");
				
				tester(text,pattern,czyPokazywacWyniki,odp);
				
			}
			else if (odp==3)
			{
				StreamWriter los1 = new StreamWriter(@"los1.txt",false);
				StreamWriter los1Pattern = new StreamWriter(@"los1Pattern.txt",false);
				los1.WriteLine(Generator.GetRandomString(10000000-2));
				los1Pattern.WriteLine(Generator.GetRandomString(6));
				los1.Close();
				los1Pattern.Close();
				text = System.IO.File.ReadAllText(@"los1.txt");
				pattern = System.IO.File.ReadAllText(@"los1Pattern.txt");
				tester(text,pattern,czyPokazywacWyniki,odp);
			}
		}
				

			Console.Write("Press any key to continue . . . ");
			Console.ReadKey(true);
		}
		
		public static void tester(string text, string pattern,
		                          bool czyPokazywacWyniki,int odp)
		{
			StreamWriter wyniki = new StreamWriter(@"odpowiedzi.txt",true);
			if (odp==1)
			{
				wyniki.Write("alfabet mówiony\t");
			}
			if (odp==2)
			{
				wyniki.Write("alfabet ubogi\t");
			}
			if (odp==3)
			{
				wyniki.Write("ciągi losowe\t");
			}
			SundaySearch ss = new SundaySearch(text,pattern,czyPokazywacWyniki);
			KMPSearch kmps = new KMPSearch(text,pattern,czyPokazywacWyniki);
			MPSearch mp = new MPSearch(text,pattern,czyPokazywacWyniki);
			SundayTwoSearch s2 = new SundayTwoSearch(text,pattern,czyPokazywacWyniki);
			Stopwatch sw = new Stopwatch();
			sw.Start();
			ss.find();
			sw.Stop();
			Console.WriteLine("Jednostek Sunday: " +  sw.ElapsedTicks);
			wyniki.Write("Sunday\t" +  sw.ElapsedTicks);
			sw.Reset();
			sw.Start();
			kmps.find();
			sw.Stop();
			Console.WriteLine("Jednostek KMP: " +  sw.ElapsedTicks);
			wyniki.Write("\tKMP: \t" +  sw.ElapsedTicks);
			sw.Reset();
			sw.Start();
			mp.find();
			sw.Stop();
			Console.WriteLine("Jednostek MP: " +  sw.ElapsedTicks);
			wyniki.Write("\tMP: \t" +  sw.ElapsedTicks);
			sw.Reset();
			sw.Start();
			s2.find();
			sw.Stop();
			Console.WriteLine("Sunday2: " +  sw.ElapsedTicks);
			wyniki.Write("\tSunday2: \t" +  sw.ElapsedTicks);
			Console.WriteLine("Text dł: " + text.Length);
			wyniki.Write("\tText dł: \t" + text.Length);
			wyniki.Write("\n");
			wyniki.Close();
			Console.WriteLine("------------------------------");
			Console.WriteLine(File.ReadAllText(@"odpowiedzi.txt"));
			Console.WriteLine("------------------------------");
		}
	}
}