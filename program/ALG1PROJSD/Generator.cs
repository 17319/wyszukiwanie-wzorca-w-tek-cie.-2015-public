﻿/*
 * Created by SharpDevelop.
 * User: b
 * Date: 2015-03-17
 * Time: 16:41
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace ALG1PROJSD
{
	/// <summary>
	/// Description of Generator.
	/// </summary>
	public class Generator
	{

		
		public Generator()
		{
		}
		public static string GetRandomString(int Length)
		{
			Random r = new Random();
			char[] ArrRandomChar = new char[Length];
			for (int i = 0; i < Length; i++)
				ArrRandomChar[i] = (char)('a' + r.Next(0, 26));
			return new string(ArrRandomChar);
		}
	}
	
}
