﻿/*
 * Created by SharpDevelop.
 * User: b
 * Date: 2015-03-17
 * Time: 05:23
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;

namespace ALG1PROJSD
{
	/// <summary>
	/// Description of SundayTwo.
	/// </summary>
	public class SundayTwoSearch : StringSearch
	{
		public SundayTwoSearch(string text, string pattern, bool czyPokazacWyniki)
			: base(text, pattern,czyPokazacWyniki)
		{
		}
		
		public List<int> find()
        {
            occurences = new List<int>();
            List<List<int>> mlis = new List<List<int>>();

            for (int i=0;i<256;i++)
            {
                mlis.Add(new List<int>());
                for (int j=0;j<256;j++)
                    mlis[i].Add(-2);
            }
            for(int j=0;j<256;j++) mlis[j][(int)pattern[0]]=-1;
            for(int i=0;i<m-1;i++) mlis[(int)pattern[i]][(int)pattern[i+1]]=i;
            for(int j=0;j<256;j++) mlis[(int)pattern[m-1]][j]=m-1;

            for (int i = 0; i < n - m-1;) {
                if(matchesAt(i)) occurences.Add(i);
                if (i == n - 1)
                    i = n;
                else
                    i += m - mlis[(int)text[i+m]][(int)text[i+m+1]];
            }

            if (czyPokazacWyniki){
                Console.Write("Wyniki SundayaTwo: ");
                show();
            }
            return occurences;
        } 
	}
}
