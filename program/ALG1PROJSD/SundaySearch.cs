﻿/*
 * Utworzone przez SharpDevelop.
 * Użytkownik: b
 * Data: 2015-02-26
 * Godzina: 05:39
 * 
 * Do zmiany tego szablonu użyj Narzędzia | Opcje | Kodowanie | Edycja Nagłówków Standardowych.
 */
using System;
using System.Collections.Generic;
namespace ALG1PROJSD
{
	/// <summary>
	/// Description of AlgSunday.
	/// </summary>
	public class SundaySearch : StringSearch
	{
		
		public SundaySearch(string text, string pattern, bool czyPokazacWyniki)
			: base(text, pattern,czyPokazacWyniki)
		{
		}
		
		
		public List<int> find()
		{
			int i;
			
			P = new List<int>(256);
			occurences = new List<int>();
			for (i=0;i<256;i++)
			{
				P.Add(-1);
			}
			for (i = 0; i <m; i++) {
				P[pattern[i]] = i;
			}
			i=0;
			while (i <= n - m) {
				if (matchesAt(i)) {
					occurences.Add(i);
				}
				i += m;
				if (i < n) {
					i -= P[text[i]];
				}
			}
			
			if (czyPokazacWyniki){
				Console.Write("Wyniki Sundaya: ");
				show();
			}
			
			return occurences;
		}
		
		
	}
}
