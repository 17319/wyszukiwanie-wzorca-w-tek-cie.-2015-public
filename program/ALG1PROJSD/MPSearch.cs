﻿/*
 * Created by SharpDevelop.
 * User: b
 * Date: 2015-03-16
 * Time: 19:15
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.IO;
namespace ALG1PROJSD
{
	public class MPSearch : StringSearch
	{
		public MPSearch(string text, string pattern, bool czyPokazacWyniki)
			: base(text, pattern,czyPokazacWyniki)
		{
			
		}
		
		public List<int> find()
		{
			int i=0;
			int j=-1;
			occurences = new List<int>(m+1);
			P = new List<int>();
			for(int iii=0;iii<m+1;iii++)
				P.Add(-1);
	
			while (i < m) {
				while(j > -1 && pattern[i] != pattern[j]) {
					j = P[j];
				}
				i++;
				j++;
				P.Add(j);
			}
			i=0; j=0;
			while (i < n) {
				while (j >= 0 && text[i] != pattern[j]) {
					j = P[j];
				}
				i++;
				j++;
				if (j == m) {
					occurences.Add(i-m);
					j = P[m];
				}
			}
			if (czyPokazacWyniki){
				Console.Write("Wyniki MP: ");
				show();
			}
			
			
			return occurences;
		}
		
		
		
	}
	
}