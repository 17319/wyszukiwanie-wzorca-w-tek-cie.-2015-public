﻿/*
 * Utworzone przez SharpDevelop.
 * Użytkownik: b
 * Data: 2015-03-08
 * Godzina: 12:02
 * 
 * Do zmiany tego szablonu użyj Narzędzia | Opcje | Kodowanie | Edycja Nagłówków Standardowych.
 */
using System;
using System.Collections.Generic;

namespace ALG1PROJSD
{
	/// <summary>
	/// Description of algMPandKMP.
	/// </summary>
	public class KMPSearch : StringSearch
	{
		
		public KMPSearch(string text, string pattern, bool czyPokazacWyniki)
			: base(text, pattern,czyPokazacWyniki)
		{
		}
		public List<int> find()
		{
			occurences = new List<int>();
			P = new List<int>(m+1);
			int i=0;
			int j=-1;
			P.Add(-1);
			while (i < m) {
				while(j > -1 && pattern[i] != pattern[j]) {
					j = P[j];
				}
				i++;
				j++;
				if (pattern[j] == pattern[i-1]) {
					P.Add(P[j]);
				} else {
					P.Add(j);
				}
			}
			i=0; j=0;
			while (i < n) {
				while (j >= 0 && text[i] != pattern[j]) {
					j = P[j];
				}
				i++;
				j++;
				if (j == m) {
					occurences.Add(i-m);
					j = P[m];
				}
			}
			if (czyPokazacWyniki){
				Console.Write("Wyniki KMP: ");
				show();
			}
			return occurences;
		}
		
		
		
		
	}
}
